<?php
include('koneksi.php');
require_once('header.php');
?>



<div class="container">
        <div class="table-wrapper"> 
            <div class="table-title" >
                <div class="row">
                    <div class="col-sm-4">
						<h2>Order <b>Details</b></h2>
					</div>
					<div class="col-sm-8">
                    <a href="data_cust.php"> <button class="btn btn-primary" id="data_cust"><i class="material-icons" >&#xE863;</i><span>Refresh List</span> </button></a>						
					<a id="downloadLink" onclick="exportF(this)" class="btn btn-info"><i class="material-icons">&#xE24D;</i> <span>Export to Excel</span></a>
					</div>
                </div>
            </div>
			<div class="table-filter display" id="example">
				<div class="row">
          <div class="col-sm-3" >
						<div class="show-entries dataTables_length" id="example_length" style="display: none;">
							<span>Show</span>
							<select class="form-control" id="example" name="example_length" aria-controls="example">
								<option value="2">2</option>
								<option value="4">4</option>
							
							</select>
							<span>entries</span>
						</div>
                    </div>


                    <form  method="POST">
                    <div class="col-sm-12">
                        
                        <div class="filter-group">
                        <input type="submit" name="cari" value="cari" class="btn btn-primary" style="width: 20%; border: 1spx solid;">
                        <select name="option" required="" style="width: 30%;"><br>
                            <option value="ID" disabled="">Cari Data</option>
                            <option value="nama">By Nama</option>
                            <option value="id_pelanggan">By Invoice</option>
                            <option value="handphone">By Handphone</option>
                            
                        </select>
                            <input type="text" class="form-control" style="width: 40%; border: 2px solid;" name="search"  placeholder="cari by">
						</div>
						    <span class="filter-icon"><i class="fa fa-filter"></i></span>
                    </div>
                    </form>
                </div>
            </div>
            
         
            <table class="table table-striped table-hover table-responsive" id="table1" >
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Customer</th>
                        <th>Elektronik</th>
                        <th>No Handphone</th>						
                        <th>Tanggal Daftar</th>						
                        <th>Tanggal Pengerjaan</th>
                        <th>Status</th>
                        <th>Kerusakan</th>
                        <th>Keterangan</th>
                        <th>Action</th>
                    </tr>
                </thead>


               
                <tbody>
                <?php
                  $no = 1;
                  if(isset($_POST['cari'])){
                    $cari = $_POST['cari'];
                    $search = $_POST['search'];
                    $option = $_POST['option'];                  
                    $hasil = mysqli_query($koneksi,"SELECT * FROM customer WHERE $option LIKE '%$search%';");
                        }
                  
                      else {
                            $hasil = mysqli_query($koneksi,"SELECT * FROM customer where status != 'selesai' order BY tanggal DESC;");
                        }
                  while($d=mysqli_fetch_array($hasil)){
                ?>

                    <tr>
                        <td data-header="ID"><?php echo $d['id_pelanggan']; ?></td>
                        <td data-header="nama"><?php echo $d['nama']; ?></td>
						<td data-header="elektronik"><?php echo $d['elektronik']; ?></td>
                        <td data-header="handphone"><?php echo $d['handphone']; ?></td>                        
                        <td data-header="tanggal daftar"><?php echo $d['tanggal']; ?></td>                        
                        <td data-header="tanggal pengerjaan"><?php echo $d['last_update']; ?></td>
                        <td data-header="status"><span class="status text-success"></span> <?php echo $d['status']; ?></td>
                        <td data-header="kerusakan"><?php echo $d['kerusakan']; ?></td>
                        <td data-header="keterangan"><?php echo $d['keterangan']; ?></td>
                        <td><a href="profile.php?id_pelanggan=<?=$d['id_pelanggan'];?>" class="view" title="View Details" data-toggle="tooltip"><i class="material-icons">&#xE5C8;</i></a></td>
                    </tr>
					
                      
                </tbody>

                <?php
                  }
                ?>
            </table>
            

			
        </div>
    </div>     


    <script >
      function exportF(elem) {
        var table = document.getElementById("table");
        var html = table.outerHTML;
        var d = new Date();
        
        var url = 'data:application/vnd.ms-excel,' + escape(html); // Set your html table into url 
        elem.setAttribute("href", url);
        elem.setAttribute("download", + d +"_export.xls" ); // Choose the file name
        return false;
      }
    </script>

<script>
        $(document).ready(function (){
            $('table1').DataTable()
        }
        )
    </script>

<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="java.js"></script>