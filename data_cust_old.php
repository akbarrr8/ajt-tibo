<?php
include('koneksi.php');
require_once('header.php');
?>

            <table class="table table-striped table-hover" id="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Customer</th>
                        <th>Elektronik</th>
                        <th>No Handphone</th>						
                        <th>Tanngal Daftar</th>						
                        <th>Tanggal Pengerjaan</th>
                        <th>Status</th>
                        <th>Kerusakan</th>
                        <th>Keterangan</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <?php
                  $no = 1;
                
                  $hasil = mysqli_query($koneksi,"SELECT * FROM customer where status != 'selesai' order BY tanggal DESC;");
                  
                  while($d=mysqli_fetch_array($hasil)){
                ?>

                <tbody>
                    <tr>
                        <td><?php echo $d['id_pelanggan']; ?></td>
                        <td><?php echo $d['nama']; ?></td>
						            <td><?php echo $d['elektronik']; ?></td>
                        <td><?php echo $d['handphone']; ?></td>                        
                        <td><?php echo $d['tanggal']; ?></td>                        
                        <td><?php echo $d['last_update']; ?></td>
                        <td><span class="status text-success">&bull;</span> <?php echo $d['status']; ?></td>
                        <td><?php echo $d['kerusakan']; ?></td>
                        <td><?php echo $d['keterangan']; ?></td>
                        <td><a href="profile.php?id_pelanggan=<?=$d['id_pelanggan'];?>" class="view" title="View Details" data-toggle="tooltip"><i class="material-icons">&#xE5C8;</i></a></td>
                    </tr>
					
                      
                </tbody>

                <?php
                  }
                ?>
            </table>
            

            <script>
        $(document).ready(function (){
            $('table').DataTable()
        }
        )
    </script>