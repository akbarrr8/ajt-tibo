<?php

  include_once('koneksi.php');
  require_once('header.php');

  if(isset($_GET['id_pelanggan'])){
    $id_pelanggan = $_GET['id_pelanggan'];
    $query = "SELECT * FROM customer WHERE id_pelanggan = '$id_pelanggan';";

    $hasil = mysqli_query($koneksi, $query);
    
?>


    <div class="container">

      <div class="starter-template text-center">
        <h1>Ari Jaya Teknik</h1>
        <p class="lead ">Data Customer</p>
        <hr>
      </div>

    </div><!-- /.container -->

    <div class="container ">
      <div class="row">
        <div class="col-md-8 col-md-offset-4" >
          <form class="form-horizontal" action="proses/edit-data.php" method="POST">
                
                <?php
                  while($data = mysqli_fetch_array($hasil)){      
                ?>

            <div class="form-group">
              <label class="col-sm-2 control-label">Nama</label>
              <div class="col-sm-4">
                <input type="text" class="form-control"  name="nama"  value="<?=$data['nama']?>">
              </div>
            </div>
            <input type="text"  name="id_pelanggan" value="<?=$data['id_pelanggan']?>" hidden="">
            <div class="form-group">
              <label class="col-sm-2 control-label">elektronik</label>
              <div class="col-sm-4">
                <input type="text" class="form-control"  name="elektronik"  value="<?=$data['elektronik']?>">
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2 control-label">handphone</label>
              <div class="col-sm-4">
                <input type="text" class="form-control"  name="handphone"  value="<?=$data['handphone']?>">
              </div>
            </div>
            
            <div class="form-group">
              <label class="col-sm-2 control-label">kerusakan</label>
              <div class="col-sm-4">
                <input type="text" class="form-control" name="kerusakan"  value="<?=$data['kerusakan']?>"> 
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2 control-label">status</label>
              <div class="col-sm-4">
                <select class="form-control" name="status"  value="" >
                  <option><?=$data['status']?></option>
                  <option value="Antrian">Antrian</option>

                  <option value="Dalam pengerjaan">Dalam Pengerjaan</option>
                  <option value="selesai">Selesai Dikerjakan</option>
                 

                </select>
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2 control-label">Keterangan</label>
              <div class="col-sm-4">
                <input type="text" class="form-control" name="keterangan"  value="<?=$data['keterangan']?>"> 
              </div>
            </div>

            <div class="form-group">
              <div class="col-md-8 col-md-offset-2">
                <div class="col-sm-4">
                <button type="submit"  name="edit" class="btn btn-info" onclick="return confirm('Anda Yakin Data di Ubah ?');">Update</button>
                </div>

                <!-- <div class="col-sm-4">
                <a  href="proses/delete.php?id_pelanggan=<?=$d['id_pelanggan'];?>" onclick="return confirm('Are you sure ?');" > <button class="btn btn-danger btn-sm">Delete</button></a>
                  
                </div> -->
                <!-- <div class="col-sm-4">
                <a class="btn btn-primary btn-sm"href="cetak-invoice.php?id_pelanggan=<?=$data['id_pelanggan'];?>">Print Invoice</a>
                </div> -->
              </div>
            </div>
     
                <?php

    }

    ?>
          </form>
        </div>
    </div>
  </div>


<?php

}
?>

