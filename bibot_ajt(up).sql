-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 10, 2020 at 06:19 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bibot_ajt`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id_pelanggan` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `elektronik` varchar(255) DEFAULT NULL,
  `handphone` varchar(255) DEFAULT NULL,
  `kerusakan` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT current_timestamp(),
  `last_update` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id_pelanggan`, `nama`, `elektronik`, `handphone`, `kerusakan`, `status`, `keterangan`, `tanggal`, `last_update`) VALUES
(35, 'akbar faisal', 'jl jlaan mulu', '08920384034', 'aman-aman aja', 'Antrian', 'Garansi', '2020-04-10 22:36:18', '2020-04-27 23:17:29'),
(38, 'fino', 'jl kali', '08293293', 'Bocor', 'selesai', '', '2020-04-10 23:07:41', '2020-05-01 11:15:30'),
(39, 'bibot', 'yakub', '09834387438', '', 'Antrian', '', '2020-04-27 19:21:27', '2020-04-27 19:25:47'),
(40, 'satr', 'hkhk', '09897686', '', 'Dalam pengerjaan', '', '2020-04-27 19:27:51', '2020-04-27 19:28:07'),
(41, 'Penyok', 'Jalak', '0283936', 'pcb panas', 'selesai', 'Sudah selesai', '2020-04-27 19:30:00', '2020-05-01 03:50:43'),
(42, 'akbar', 'AC (panasonic)', '08934783', 'PCB rusak , tolong di perbaiki', 'antrian', '', '2020-04-27 22:56:44', NULL),
(43, 'panjul', 'mesin cuci', '0892349', 'gpp\r\n', 'antrian', '', '2020-04-29 21:35:37', NULL),
(45, 'ira', 'AC (sanken)', '08934893', 'panas', 'antrian', '', '2020-04-29 21:37:25', NULL),
(46, 'Sulis', 'Ac Changhong', '089238237', 'Tidak berat', 'Antrian', '', '2020-05-01 03:39:20', '2020-05-01 03:42:20'),
(47, 'Mandra', 'Mesin cuci Sanken', '0892372837', 'Iseng', 'antrian', '', '2020-05-01 03:41:39', NULL),
(48, 'Faisal', 'Vape', '+628438473', 'koilnya abis', '', 'tolong benerin', '2020-05-03 12:25:39', NULL),
(54, 'faisal', 'test', '089324890', 'jsdhak', '', 'mcnz,xm', '2020-05-05 22:24:11', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id_pelanggan`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id_pelanggan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
